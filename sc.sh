#!/bin/bash

nasm -f elf32 -o $1.o $1.asm
ld -m elf_i386 -o $1 $1.o

for i in $(objdump -d ./$1 |grep "^ " |cut -f2); do echo -n '\x'$i >> sc.txt; done; echo
cat sc.txt | sed 's/^/"/' | sed 's/$/"/g'
printf "\n\n"

if cat sc.txt | grep 00; then
	echo "[-] Null bytes in the shellcode!"
fi

rm sc.txt
